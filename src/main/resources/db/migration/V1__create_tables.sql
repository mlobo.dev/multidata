create sequence seq_user start with 1 increment by 1;

create table users
(
    cod_user bigint not null default nextval('seq_user'),
    cpf varchar(11),
    name varchar(255),
    email varchar(255),
    login varchar(255),
    createdAt timestamp,
    primary key(cod_user)
);