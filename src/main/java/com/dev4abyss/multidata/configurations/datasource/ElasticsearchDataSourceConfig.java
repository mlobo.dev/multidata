package com.dev4abyss.multidata.configurations.datasource;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.dev4abyss.multidata.module.elastic")
@ComponentScan(basePackages = { "com.dev4abyss.multidata.module.elastic" })
public class ElasticsearchDataSourceConfig {

    @Value("${elasticsearch.url}")
    String elasticsearchUrl;

    @Value("${elasticsearch.connect-timeout}")
    Long elasticsearchConnectTimeout;

    @Value("${elasticsearch.socket-timeout}")
    Long elasticsearchSocketTimeout;

    @Bean
    public RestHighLevelClient client() {
        ClientConfiguration clientConfiguration= ClientConfiguration.builder()
                .connectedTo(elasticsearchUrl)
                .withConnectTimeout(elasticsearchConnectTimeout)
                .withSocketTimeout(elasticsearchSocketTimeout)
                .build();

        return RestClients.create(clientConfiguration).rest();
    }

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() {
        return new ElasticsearchRestTemplate(client());
    }
}
