package com.dev4abyss.multidata.configurations.datasource;


import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import static com.dev4abyss.multidata.shared.utils.StringUtils.isEmptyOrNull;

@Configuration
@EnableMongoRepositories(basePackages = "com.dev4abyss.multidata.module.mongo")
@ComponentScan(basePackages = { "com.dev4abyss.multidata.module.mongo" })
public class MongoDbDataSourceConfig {

    @Value("${mongodb.url}")
    String url;

    @Value("${mongodb.database}")
    String database;

    @Value("${mongodb.username}")
    String username;

    @Value("${mongodb.password}")
    String password;

    @Bean
    public MongoClient mongo() {
        String urlConexao;
        if(!isEmptyOrNull(username) && !isEmptyOrNull(password)){
            urlConexao = "mongodb://" + username + ":" + password + "@" + url + "/" + ( database != null ? database : "" );
        }
        else {
            urlConexao = "mongodb://" + url + "/" + ( database != null ? database : "" );
        }
        ConnectionString connectionString = new ConnectionString(urlConexao);
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();

        return MongoClients.create(mongoClientSettings);
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongo(), database);
    }
}
