package com.dev4abyss.multidata.module.postgres.user.service;

import com.dev4abyss.multidata.exceptions.ObjectNotFoundException;
import com.dev4abyss.multidata.module.postgres.user.dto.UserDTO;
import com.dev4abyss.multidata.module.postgres.user.entity.User;
import com.dev4abyss.multidata.module.postgres.user.mapper.UserMapper;
import com.dev4abyss.multidata.module.postgres.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper mapper;

    public User save(UserDTO dto) {
        return userRepository.save(mapper.toEntity(dto));
    }

    public User findById(Long id) {
        return userRepository.findById(id).orElseThrow(
                () -> new ObjectNotFoundException("User not found by id: " + id)
        );
    }
}
