package com.dev4abyss.multidata.module.postgres.user.controller;

import com.dev4abyss.multidata.module.postgres.user.dto.UserDTO;
import com.dev4abyss.multidata.module.postgres.user.mapper.UserMapper;
import com.dev4abyss.multidata.module.postgres.user.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
@Tag(name = "Users", description = "Users Resources from postgres ")
public class UserController {


    private final UserService userService;
    private final UserMapper userMapper;


    @PostMapping
    public ResponseEntity<UserDTO> save(@RequestBody UserDTO dto) {
        return ResponseEntity.ok(userMapper.toDto(userService.save(dto)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok(userMapper.toDto(userService.findById(id)));
    }
}
