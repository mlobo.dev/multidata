package com.dev4abyss.multidata.module.postgres.user.mapper;


import com.dev4abyss.multidata.module.postgres.user.dto.UserDTO;
import com.dev4abyss.multidata.module.postgres.user.entity.User;
import com.dev4abyss.multidata.shared.base.BaseMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper extends BaseMapper<User, UserDTO> {

}
