package com.dev4abyss.multidata.module.postgres.user.dto;


import com.dev4abyss.multidata.shared.base.BaseDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = false)
@Data
public class UserDTO extends BaseDTO {
    private Long id;
    private String login;
    private String cpf;
    private String name;
    private String email;
    private LocalDateTime createdAt;

}
