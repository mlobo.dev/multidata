package com.dev4abyss.multidata.module.postgres.user.repository;

import com.dev4abyss.multidata.module.postgres.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
