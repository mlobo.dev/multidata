package com.dev4abyss.multidata.module.mongo.address.mapper;


import com.dev4abyss.multidata.module.mongo.address.dto.AddressDTO;
import com.dev4abyss.multidata.module.mongo.address.entity.Address;
import com.dev4abyss.multidata.module.postgres.user.dto.UserDTO;
import com.dev4abyss.multidata.module.postgres.user.entity.User;
import com.dev4abyss.multidata.shared.base.BaseMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AddressMapper extends BaseMapper<Address, AddressDTO> {

}
