package com.dev4abyss.multidata.module.mongo.address.controller;


import com.dev4abyss.multidata.module.mongo.address.dto.AddressDTO;
import com.dev4abyss.multidata.module.mongo.address.mapper.AddressMapper;
import com.dev4abyss.multidata.module.mongo.address.service.AddressService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/address")
@Tag(name = "Address", description = "Address resources from mongo db")
public class AddressController {


    private final AddressService userService;
    private final AddressMapper userMapper;


    @PostMapping
    public ResponseEntity<AddressDTO> save(@RequestBody AddressDTO dto) {
        return ResponseEntity.ok(userMapper.toDto(userService.save(dto)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AddressDTO> findById(@PathVariable String id) {
        return ResponseEntity.ok(userMapper.toDto(userService.findById(id)));
    }
}
