package com.dev4abyss.multidata.module.mongo.address.service;

import com.dev4abyss.multidata.exceptions.ObjectNotFoundException;
import com.dev4abyss.multidata.module.mongo.address.dto.AddressDTO;
import com.dev4abyss.multidata.module.mongo.address.entity.Address;
import com.dev4abyss.multidata.module.mongo.address.mapper.AddressMapper;
import com.dev4abyss.multidata.module.mongo.address.repository.AddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class AddressService {

    private final AddressRepository userRepository;
    private final AddressMapper mapper;

    public Address save(AddressDTO dto) {
        return userRepository.save(mapper.toEntity(dto));
    }

    public Address findById(String id) {
        return userRepository.findById(id).orElseThrow(
                () -> new ObjectNotFoundException("Address not found by id: " + id)
        );
    }
}
