package com.dev4abyss.multidata.module.mongo.address.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Document(collection = "address")
public class Address {

    @Id
    private String id;
    private String city;
    private String description;


}