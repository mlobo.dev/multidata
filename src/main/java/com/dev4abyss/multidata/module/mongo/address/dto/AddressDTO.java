package com.dev4abyss.multidata.module.mongo.address.dto;

import com.dev4abyss.multidata.shared.base.BaseDTO;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@EqualsAndHashCode(callSuper = true)
@Data
public class AddressDTO extends BaseDTO {

    private String id;
    private String city;
    private String description;

}