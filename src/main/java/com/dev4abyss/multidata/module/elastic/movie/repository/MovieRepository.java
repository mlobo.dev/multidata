package com.dev4abyss.multidata.module.elastic.movie.repository;

import com.dev4abyss.multidata.module.elastic.movie.entity.Movie;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends ElasticsearchRepository<Movie, Long> {
}
