package com.dev4abyss.multidata.module.elastic.movie.entity;

import lombok.*;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.Id;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Document(indexName = "movies")
public class Movie {

    @Id
    private Long id;

    private String title;

    private Integer year;

    private Set<String> genre = new HashSet<>();

}
