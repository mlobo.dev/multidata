package com.dev4abyss.multidata.module.elastic.movie.service;

import com.dev4abyss.multidata.exceptions.ObjectNotFoundException;
import com.dev4abyss.multidata.module.elastic.movie.entity.Movie;
import com.dev4abyss.multidata.module.elastic.movie.repository.MovieRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class MovieService {

    private final MovieRepository repository;

    public Movie findById(Long id) {
        return repository.findById(id).orElseThrow(
                () -> new ObjectNotFoundException("Movie not found by id: " + id)
        );
    }
}
