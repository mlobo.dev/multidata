package com.dev4abyss.multidata.module.elastic.movie.controller;


import com.dev4abyss.multidata.module.elastic.movie.dto.MovieDTO;
import com.dev4abyss.multidata.module.elastic.movie.mapper.MovieMapper;
import com.dev4abyss.multidata.module.elastic.movie.service.MovieService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/movies")
@Tag(name = "Movies", description = "Movies resources from elastic search")
public class MovieController {


    private final MovieService service;
    private final MovieMapper mapper;


    @GetMapping("/{id}")
    public ResponseEntity<MovieDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok(mapper.toDto(service.findById(id)));
    }
}
