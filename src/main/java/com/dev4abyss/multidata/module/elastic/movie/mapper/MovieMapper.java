package com.dev4abyss.multidata.module.elastic.movie.mapper;


import com.dev4abyss.multidata.module.elastic.movie.dto.MovieDTO;
import com.dev4abyss.multidata.module.elastic.movie.entity.Movie;
import com.dev4abyss.multidata.shared.base.BaseMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MovieMapper extends BaseMapper<Movie, MovieDTO> {

}
