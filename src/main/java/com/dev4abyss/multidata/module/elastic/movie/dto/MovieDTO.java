package com.dev4abyss.multidata.module.elastic.movie.dto;

import com.dev4abyss.multidata.shared.base.BaseDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
public class MovieDTO extends BaseDTO {

    private Long id;
    private String title;
    private Integer year;
    private Set<String> genre = new HashSet<>();

}
