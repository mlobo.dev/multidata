package com.dev4abyss.multidata.shared.base;

import lombok.Data;

import java.io.Serializable;

@Data
public abstract class BaseDTO implements Serializable {
    private static final long serialVersionUID = 1357668119881911675L;
}
