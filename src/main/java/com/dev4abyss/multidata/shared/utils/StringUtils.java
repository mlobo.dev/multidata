package com.dev4abyss.multidata.shared.utils;


import lombok.SneakyThrows;

import javax.swing.text.MaskFormatter;

public class StringUtils {


    public static boolean isEmptyOrNull(String s) {
        return s == null || org.springframework.util.StringUtils.isEmpty(s);
    }

    public static String removerCaracteresEspeciaisCPF(String cpf) {
        return cpf.replaceAll("[^\\d ]", "");
    }

    @SneakyThrows
    public static String formatarCpf(String cpf) {
        MaskFormatter mask = new MaskFormatter("###.###.###-##");
        mask.setValueContainsLiteralCharacters(false);
        return mask.valueToString(cpf);
    }
    private StringUtils() {
    }
}
